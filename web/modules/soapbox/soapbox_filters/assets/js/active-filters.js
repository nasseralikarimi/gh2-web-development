/**
 * Active filters tasks.
 */
(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.soapboxActiveFilters = {
    attach: function (context, drupalSettings) {
      if (!drupalSettings.autoSubmit) {
        const $form         = $("form[id*='views-exposed-form']");
        const $submitButton = $form.find('.js-form-submit')
        let BoxId, $el;

        $('.js-active-filter', context).once().on('click', function (e) {
          BoxId = $(this).attr('for');
          $el = $("[id*='" + BoxId + "']");

          if ($el.is(':checkbox,:radio')) {
            $el.prop('checked', false);
          }
          else if ($el.is(':text')) {
            let val = $el.val();
            val = val.replace($(this).text(), "");
            val = val.replace(/(^\s*,\s*)|(\s*,\s*$)/, "");
            $el.val(val);
          }

          // Remove active filter label
          $(this).remove();

          // Uncheck the filter checkbox
          // Submit the exposed form
          // $form.submit();
          $submitButton.trigger('click');
        });
      }
    }
  };
})(jQuery, Drupal);
