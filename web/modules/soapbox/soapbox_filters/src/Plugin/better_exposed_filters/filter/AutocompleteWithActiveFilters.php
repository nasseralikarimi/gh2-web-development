<?php

namespace Drupal\soapbox_filters\Plugin\better_exposed_filters\filter;

use Drupal\better_exposed_filters\Plugin\better_exposed_filters\filter\DefaultWidget;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Form\FormStateInterface;

/**
 *  Autocomplete with active filters widget implementation.
 *
 * @BetterExposedFiltersFilterWidget(
 *   id = "bef_soapbox_autocomplete_active_filters",
 *   label = @Translation("Autocomplete with Active Filters"),
 * )
 */
class AutocompleteWithActiveFilters extends DefaultWidget {

  use BefActiveFilters;

  /**
   * {@inheritdoc}
   */
  public function exposedFormAlter(array &$form, FormStateInterface $form_state) {
    parent::exposedFormAlter($form, $form_state);

    $filter = $this->handler;
    // Form element is designated by the element ID which is user-
    // configurable.
    $field_id = $filter->options['is_grouped'] ? $filter->options['group_info']['identifier'] : $filter->options['expose']['identifier'];
    $input = $form_state->getUserInput();
    // Don't generate active filters if.
    if (empty($form[$field_id])
      || !$this->configuration['show_active_filters']
      || empty($input[$field_id])) {
      return;
    }

    $input_values = Tags::explode($input[$field_id]);
    foreach ($input_values as $input_value) {
      $items[] = [
        'title' => $input_value,
        'value' => $input_value,
      ];
    }

    if (!empty($items)) {
      $this->addActiveFilter($field_id, $items, $form);
    }
  }

}
