<?php

namespace Drupal\unified_date\Form;

/**
 * @file
 * Contains \Drupal\unified_date\Form\SettingsForm.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\FieldConfigInterface;

/**
 * Configure menu link weight settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'unified_date_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unified_date.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config              = $this->config('unified_date.settings');
    $entity_type_manager = \Drupal::entityTypeManager();

    $node_types = $entity_type_manager
      ->getStorage('node_type')
      ->loadMultiple();
    if ($node_types) {
      foreach (array_keys($node_types) as $node_type) {
        $form[$node_type] = [
          '#type'     => 'select',
          '#options'  => [
            'base-field:created' => 'base-field:created',
            'base-field:changed' => 'base-field:changed',
          ] + $this->getNodeDateFields($node_type),
          '#title'    => $this->t('Node type "@node_type"', [
            '@node_type' => $node_type,
          ]),
          '#default_value' => $config->get($node_type),
          '#description' => $this->t('All node types without date fields selected will used the "Created" date.'),
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Get node fields with date type.
   *
   * @param string $contact_form_id
   *   The ID of the contact form.
   *
   * @return mixed
   *   The array of date field keys.
   */
  public function getNodeDateFields($node_type) {
    $available_fields = [];

    // Get all field attached to a particular node type.
    $entity_field_manager = \Drupal::service('entity_field.manager');
    $fields               = array_filter(
      $entity_field_manager->getFieldDefinitions('node', $node_type),
      function ($field_definition) {

        // Ensure this is a config based field and not a base entity field.
        return $field_definition instanceof FieldConfigInterface;
      }
    );
    if ($fields) {
      /** @var \Drupal\Core\Field\FieldDefinitionInterface $field */
      foreach ($fields as $field) {
        $type = $field->getType();

        // SBTODO maybe add date range if needed in future project?
        if (in_array($type, ['datetime','timestamp'])) {
          $available_fields[$field->getName()] = $field->getName();
        }
      }
    }

    return $available_fields;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config              = $this->config('unified_date.settings');
    $entity_type_manager = \Drupal::entityTypeManager();

    // Loop through node types and store field selection in config.
    $node_types = $entity_type_manager
      ->getStorage('node_type')
      ->loadMultiple();
    if ($node_types) {
      foreach (array_keys($node_types) as $node_type) {
        $config->set($node_type, $form_state->getValue($node_type));
      }
    }

    // Save the config.
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
