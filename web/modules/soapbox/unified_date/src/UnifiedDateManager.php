<?php

namespace Drupal\unified_date;

use \Drupal\node\NodeInterface;

/**
 * Class UnifiedDateManager.
 */
class UnifiedDateManager {

  /**
   * Raw unified date settings config.
   *
   * @var array
   */
  private $config_raw_data = [];

  /**
   * UnifiedDateManager constructor.
   */
  public function __construct() {

    $config = \Drupal::config('unified_date.settings');
    $this->config_raw_data = $config->getRawData();
  }

  /**
   * Get node unified date.
   *
   * @param NodeInterface $node
   *   The node object.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getUnifiedDate(NodeInterface $node) {

    $node_type = $node->getType();

    // Field where date is stored.
    if (isset($this->config_raw_data[$node_type])) {
      $field = $this->config_raw_data[$node_type];
    }
    else {
      $field = 'base-field:created';
    }

    // Determine date.
    switch ($field) {
      case 'base-field:changed':
        $date = $node->getChangedTime();
        break;

      default:
        if ($node->hasField($field)) {
          if ($node->getFieldDefinition($field)->getType() !== 'timestamp') {
            $date = strtotime($node->get($field)->value);
          }
          else {
            $date = $node->get($field)->value;
          }
        }
        break;
    }

    // Set Default to created time if unified date is empty.
    if (empty($date)) {
      $date = $node->getCreatedTime();
    }

    return $date;
  }

  /**
   * Set node unified date.
   *
   * @param NodeInterface $node
   *   The node object.
   * @param bool $save
   *   Whether or not to save the node.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setNodeUnifiedDate(NodeInterface $node) {

    $node->unified_date = $this->getUnifiedDate($node);
    $node->save();
  }

}
