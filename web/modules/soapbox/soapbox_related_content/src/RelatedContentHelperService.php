<?php

namespace Drupal\soapbox_related_content;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;

/**
 * Class RelatedContentHelperService.
 */
class RelatedContentHelperService {

  /**
   * Drupal\Core\Entity\Query\QueryFactory definition.
   *
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Render\RendererInterface definition.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Allowed node types.
   *
   * @var array
   */
  protected $allowed_types = [];

  /**
   * Manual selections.
   *
   * @var array
   */
  protected $manual_selections = [];

  /**
   * Determine if it is page content type.
   *
   * @var array
   */
  protected $page_content_type = FALSE;

  /**
   * Fields to check.
   * This can be entity reference or taxonomy reference fields.
   *
   * @var array
   */
  protected $fields_to_check = [];

  /**
   * Other fields to check.
   *
   * This can be entity reference or taxonomy reference fields.
   *
   * @var array
   */
  protected $crossreference_fields_to_check = [];

  /**
   * The field names to check for matches based on existing node references.
   *
   * @var array
   */
  protected $entityIdsToMatchByField = [];

  /**
   * Ids to exclude from the entities searched.
   *
   * @var array
   */
  protected $excludedIds = [];

  /**
   * View mode to render into.
   *
   * @var array
   */
  protected $view_mode = '';

  /**
   * Return limit.
   *
   * @var int
   */
  protected $limit = 4;

  /**
   * Store sort orders with default as unified date descending.
   *
   * @var \string[][]
   */
  protected $sortOrders = [
    [
      'field' => 'unified_date',
      'direction' => 'DESC',
    ],
  ];

  /**
   * Constructs a new RelatedContentHelperService object.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param RendererInterface          $renderer
   *   Renderer.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer
  ) {

    $this->entityTypeManager = $entity_type_manager;
    $this->renderer          = $renderer;
  }

  /**
   * Add Ids of entities to get excluded.
   *
   * @param array $ids
   *   The additional IDs to exclude.
   */
  public function addExcludedIds(array $ids) {
    $this->excludedIds = array_merge($this->excludedIds, $ids);
    $this->excludedIds = array_unique($this->excludedIds);
  }

  /**
   * Set the allowed node types.
   *
   * @param array $allowed_types
   *   An array of machine names of node types.
   */
  public function setAllowedTargetNodeTypes(array $allowed_types) {

    $this->allowed_types = $allowed_types;
  }

  /**
   * Set the field names to check for matches based on existing node references.
   *
   * This checks these fields on the current node and finds their references,
   * then uses those references to find related content.
   *
   * @param array $fields_to_check
   *   An array of machine names of fields.
   */
  public function setFieldsToCheck(array $fields_to_check) {

    $this->fields_to_check = $fields_to_check;
  }

  /**
   * Set the field names to check for matches based on existing node references.
   *
   * This checks these fields on the current node and finds their references,
   * then uses those references to find related content. This overwrites
   * if you have already added the field.
   *
   * @param string $field_name
   *   The field name on the potential related content that would reference
   *   these entity ids. Eg, 'field_authors' on a publication, when providing
   *   an array of author entity ids.
   * @param array $entity_ids
   *   An array of entity IDs.
   */
  public function setSpecificEntityIdsToMatchByField($field_name, array $entity_ids) {

    $this->entityIdsToMatchByField[$field_name] = $entity_ids;
  }

  /**
   * Set cross-reference fields to check.
   *
   * These are fields on other contents that may reference this content. For
   * instance, people that have a parent 'project', adding 'field_project' - a
   * field on the person entity - would find people where field_project matches
   * the current node ID.
   *
   * @param array $fields_to_check
   *   An array of machine names of fields.
   */
  public function setCrossReferenceFieldsToCheck(array $fields_to_check) {

    $this->crossreference_fields_to_check = $fields_to_check;
  }

  /**
   * Set manual selections.
   *
   * @param array $nids
   *   An array of node nids.
   */
  public function setManualSelections(array $nids) {

    $this->manual_selections = $nids;
  }

  /**
   * Determine page content type.
   *
   * @param bool $page_content_type
   *   TRUE if content type is page.
   */
  public function setPageContentType($page_content_type) {

    $this->page_content_type = $page_content_type;
  }

  /**
   * Set the view mode to render.
   *
   * @param string $view_mode
   *   Machine name of the view mode to render.
   */
  public function setViewMode($view_mode) {

    $this->view_mode = $view_mode;
  }

  /**
   * Set the limit of results.
   *
   * @param int $limit
   *   The number of results to return.
   */
  public function setReturnLimit($limit) {

    $this->limit = $limit;
  }

  /**
   * Set the sort orders.
   *
   * @param array $sort_orders
   *   An array of sort orders with keys, field and direction.
   *   Eg.
   *   @code
   *   Array
   *   (
   *     Array
   *     (
   *       [field] => unified_date
   *       [direction] => DESC
   *     )
   *   )
   *   @endcode
   */
  public function setSortOrders(array $sort_orders) {

    $this->sortOrders = $sort_orders;
  }

  /**
   * Handle preprocess Node for related content.
   *
   * @param array $variables
   *   Variables from hook_preprocess_node().
   *
   * @throws \Exception
   */
  public function preprocessNode(&$variables) {

    // Only preprocess full view, otherwise it will be endless loop as we render
    // the nested related nodes.
    if ($variables['view_mode'] == 'full') {
      /** @var \Drupal\node\NodeInterface $node */
      $node = $variables['node'];

      // Pass the variable back to the template. You can use this as follows:
      // {% for related_node in related_nodes %}{{ related_node }}{% endforeach %}
      if ($related_nodes = $this->getRelatedNodes($node)) {
        $variables['related_nodes'] = $this->renderSelections($related_nodes);
      }
    }
  }

  /**
   * Get all related node entities.
   *
   * This includes nodes matching the same terms and other specified entity
   * references, as well as cross referenced nodes.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The current node being viewed.
   *
   * @return \Drupal\node\NodeInterface[]
   *   An array of related node entities.
   */
  public function getRelatedNodes(NodeInterface $node) {
    $related_nodes = [];

    // Get manually selected contents.
    if (!empty($this->manual_selections)) {
      $related_nodes = Node::loadMultiple($this->manual_selections);
    }

    // Determine related contents matching specific entity ids.
    if (!empty($this->entityIdsToMatchByField)) {
      $related_nodes = $this->getRelatedNodesMatchingSpecificIdsByField($node, $related_nodes);
    }

    // Determine related contents based on tags or other entity reference
    // targets of the current node.
    if (!empty($this->fields_to_check)) {
      $node_references = $this->getNodeReferences($node);
      $related_nodes   = $this->getRelatedNodesMatchingNodeReferences($node, $node_references, $related_nodes);
    }

    // Get contents that reference this content.
    if (!empty($this->crossreference_fields_to_check)) {
      $related_nodes = $this->getCrossReferencedNodesForCurrentNode($node, $related_nodes);
    }

    // Limit (don't slice if -1, ie, unlimited).
    if ($related_nodes && count($related_nodes) > $this->limit && $this->limit > 0) {
      $related_nodes = array_slice($related_nodes, 0, $this->limit);
    }

    return $related_nodes;
  }

  /**
   * Get related nodes matching specific entity ids in specific fields.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The current node being viewed.
   * @param array $related_nodes
   *   An array of related nodes to add to.
   */
  protected function getRelatedNodesMatchingSpecificIdsByField(NodeInterface $node, array $related_nodes) {
    $storage = $this->entityTypeManager->getStorage('node');
    $query = $storage->getQuery();
    $query->condition('status', Node::PUBLISHED);

    // Must be one of allowed content types.
    $query->condition('type', $this->allowed_types, 'IN');

    // Must not be the current node or one we already have.
    $skip_nids = [
      $node->id(),
    ];
    foreach ($related_nodes as $related_node) {
      $skip_nids[] = $related_node->id();
    }
    $this->addExcludedIds($skip_nids);

    $query->condition('nid', $this->excludedIds, 'NOT IN');

    // Filter by the specific entity ids for each field.
    foreach ($this->entityIdsToMatchByField as $field_name => $entity_ids) {
      $query->condition($field_name, $entity_ids, 'IN');
    }

    foreach ($this->sortOrders as $sort_order) {
      $query->sort($sort_order['field'], $sort_order['direction']);
    }

    // Set an arbitrary limit to prevent related content comparisons from
    // loading too many nodes into memory for count comparison.
    // If the requested number of related contents is higher than the
    // arbitrary limit, set the limit to that.
    if ($this->limit == -1) {
      $limit = 25;
    }
    else {
      $limit = max(25, $this->limit);
    }
    $query->range(0, $limit);

    // Allow other modules to alter the query.
    \Drupal::moduleHandler()->alter('soapbox_related_content_specific_ids_by_field', $query);

    // Get results.
    if ($related_node_ids = $query->execute()) {
      $found_nodes = $storage->loadMultiple($related_node_ids);
      $found_nodes = $this->orderRelatedNodesByMatches(
        $found_nodes,
        $this->entityIdsToMatchByField
      );
      return array_merge($related_nodes, $found_nodes);
    }
  }

  /**
   * Get the referenced ids.
   *
   * @param object $node
   *   The node entity.
   *
   * @return array
   *   The term ids in an associative array by field machine name.
   */
  protected function getNodeReferences($node) {

    // Build storage array for references.
    $node_references = [];
    foreach ($this->fields_to_check as $field_to_check) {
      $node_references[$field_to_check] = [];
    }

    // Loop through references.
    foreach (array_keys($node_references) as $field_machine_name) {
      if (!$node->hasField($field_machine_name)) {
        continue;
      }

      // Load the field values.
      $field = $node->get($field_machine_name);
      if ($field->count() && $references = $field->getValue()) {

        // Get the target ids from the value.
        foreach ($references as $reference) {
          if (!is_array($reference) || !isset($reference['target_id'])) {
            continue;
          }

          $node_references[$field_machine_name][] = $reference['target_id'];
        }
      }
    }

    return $node_references;
  }

  /**
   * Get related contents matching same fields.
   *
   * @param object $node
   *   The node entity.
   * @param array  $node_references
   *   The array of reference entity ids from the original node.
   * @param array  $related_nodes
   *   Any related nodes already found.
   *
   * @return array
   *   The node objects.
   * @throws \Exception
   */
  protected function getRelatedNodesMatchingNodeReferences(
    $node,
    $node_references,
    array $related_nodes
  ) {

    $storage = $this->entityTypeManager->getStorage('node');

    // Get related node ids.
    $related_node_ids = [];
    foreach ($node_references as $field => $target_ids) {
      if (!$target_ids) {
        continue;
      }
      $query = $storage->getQuery();
      $query->condition('status', Node::PUBLISHED);

      // Must have at least 1 match.
      $query->condition($field, $target_ids, 'IN');

      // Must be one of allowed content types.
      $query->condition('type', $this->allowed_types, 'IN');

      // Must not be the current node or one we already have.
      $skip_nids = [
        $node->id(),
      ];
      foreach ($related_nodes as $related_node) {
        $skip_nids[] = $related_node->id();
      }
      $this->addExcludedIds($skip_nids);

      $query->condition('nid', $this->excludedIds, 'NOT IN');

      // Set an arbitrary limit to prevent related content comparisons from
      // loading too many nodes into memory for count comparison.
      // If the requested number of related contents is higher than the
      // arbitrary limit, set the limit to that.
      if ($this->limit == -1) {
        $limit = 25;
      }
      else {
        $limit = max(25, $this->limit);
      }
      $query->range(0, $limit);

      foreach ($this->sortOrders as $sort_order) {
        $query->sort($sort_order['field'], $sort_order['direction']);
      }

      // Allow other modules to alter the query.
      \Drupal::moduleHandler()->alter('soapbox_related_content_auto_query_references', $query);

      // Get results.
      $results = $query->execute();
      if ($results) {
        $related_node_ids = array_merge($related_node_ids, $results);
      }
    }

    // If related node ids count is less than the limit, get more with
    // the same content type as this node.
    if (!$this->page_content_type && count($related_node_ids) < $this->limit || $this->limit == -1) {
      $exclusions   = $related_node_ids;
      $exclusions[] = $node->id();
      $query        = $storage->getQuery();
      $query->condition('status', Node::PUBLISHED);
      $query->condition('type', $node->getType(), '=');
      $query->condition('nid', $exclusions, 'NOT IN');
      if ($this->limit > 0) {
        $limit = ($this->limit - count($related_node_ids));
      }
      $query->range(0, $limit);

      foreach ($this->sortOrders as $sort_order) {
        $query->sort($sort_order['field'], $sort_order['direction']);
      }

      // Allow other modules to alter the query.
      \Drupal::moduleHandler()->alter('soapbox_related_content_auto_query_same_type', $query);

      // Get results.
      $results = $query->execute();
      if ($results) {
        $related_node_ids = array_merge($related_node_ids, $results);
      }
    }

    // If related node ids count is less than the limit, get more with
    // just content type restrictions.
    if (count($related_node_ids) < $this->limit || $this->limit == -1) {
      $exclusions   = $related_node_ids;
      $exclusions[] = $node->id();
      $query        = $storage->getQuery();
      $query->condition('status', Node::PUBLISHED);
      $query->condition('type', $this->allowed_types, 'IN');
      $query->condition('nid', $exclusions, 'NOT IN');
      if ($this->limit > 0) {
        $limit = ($this->limit - count($related_node_ids));
      }
      $query->range(0, $limit);

      // Allow other modules to alter the query.
      \Drupal::moduleHandler()->alter('soapbox_related_content_auto_query_types', $query);

      // Get results.
      $results = $query->execute();
      if ($results) {
        $related_node_ids = array_merge($related_node_ids, $results);
      }
    }

    if ($related_node_ids) {

      $found_nodes = $storage->loadMultiple($related_node_ids);
      $found_nodes = $this->orderRelatedNodesByMatches(
        $found_nodes,
        $node_references
      );
      return array_merge($related_nodes, $found_nodes);
    }

    return $related_nodes;
  }

  /**
   * Get related contents tagged by the current node - Ex: person, partner
   *
   * @param object $node
   *   The node entity.
   * @param array  $related_nodes
   *   Any related nodes already found.
   *
   * @return array
   *   The node objects.
   * @throws \Exception
   */
  protected function getCrossReferencedNodesForCurrentNode(
    $node,
    array $related_nodes
  ) {

    if (!empty($this->crossreference_fields_to_check)) {
      $storage = $this->entityTypeManager->getStorage('node');

      // Get related node ids.
      $related_node_ids = [];

      $query = $storage->getQuery();
      $query->condition('status', Node::PUBLISHED);

      // The current node must be in the fields.
      foreach ($this->crossreference_fields_to_check as $field) {
        $query->condition($field, [$node->id()], 'IN');
      }

      // Must be one of allowed content types.
      $query->condition('type', $this->allowed_types, 'IN');

      // Must not be the current node or one we already have.
      $skip_nids = [
        $node->id(),
      ];
      foreach ($related_nodes as $related_node) {
        $skip_nids[] = $related_node->id();
      }
      $this->addExcludedIds($skip_nids);

      $query->condition('nid', $this->excludedIds, 'NOT IN');

      // Set an arbitrary limit to prevent related content comparisons from
      // loading too many nodes into memory for count comparison.
      // If the requested number of related contents is higher than the
      // arbitrary limit, set the limit to that.
      if ($this->limit == -1) {
        $limit = 25;
      }
      else {
        $limit = max(25, $this->limit);
      }
      $query->range(0, $limit);

      foreach ($this->sortOrders as $sort_order) {
        $query->sort($sort_order['field'], $sort_order['direction']);
      }

      // Get results.
      $results = $query->execute();
      if ($results) {
        $related_node_ids = array_merge($related_node_ids, $results);
      }

      if ($related_node_ids) {
        return array_merge($related_nodes,
          $storage->loadMultiple($related_node_ids));
      }
    }

    return $related_nodes;
  }

  /**
   * @param $related_nodes
   * @param $node_references
   *
   * @return array
   */
  protected function orderRelatedNodesByMatches(
    $related_nodes,
    $node_references
  ) {

    $node_results = [];
    $match_counts = [];
    if ($related_nodes) {
      foreach ($related_nodes as $related_node) {
        $match_count = $this->getMatchCount($related_node, $node_references);

        // Build nested array within match counts.
        if (!isset($match_counts[$match_count])) {
          $match_counts[$match_count] = [];
        }
        $match_counts[$match_count][] = $related_node;
      }
    }

    // Sort by match counts with highest count first.
    krsort($match_counts);

    // Flatten array in the new order.
    foreach ($match_counts as $match_count => $nodes) {
      foreach ($nodes as $node) {
        $node_results[] = $node;
      }
    }

    return $node_results;
  }

  /**
   * Get number of matches.
   *
   * @param object $related_node
   *   The node entity being compared to the original.
   * @param array $node_references
   *   The array of reference entity ids from the original node.
   *
   * @return int
   *   The number of matches.
   * @throws \Exception
   */
  protected function getMatchCount($related_node, $node_references) {

    $count = 0;
    foreach ($node_references as $field_machine_name => $target_ids) {
      if (!$related_node->hasField($field_machine_name)) {
        continue;
      }

      // Load the field values.
      $field = $related_node->get($field_machine_name);
      if ($field->count() && $references = $field->getValue()) {

        // Get the target ids from the value.
        foreach ($references as $reference) {
          if (!is_array($reference) || !isset($reference['target_id'])) {
            continue;
          }

          if (in_array($reference['target_id'], $target_ids)) {
            $count++;
          }
        }
      }
    }

    return $count;
  }

  /**
   * Run the drupal renderer service on the nodes with featured box view mode.
   *
   * @param $nodes
   *   An array of nodes.
   * @param $view_mode
   *   The view mode.
   *
   * @return array
   *   An array of rendered output.
   * @throws \Exception
   */
  protected function renderSelections($nodes) {

    $outputs = [];
    if (!$nodes) {
      return $outputs;
    }

    $view_builder = $this->entityTypeManager->getViewBuilder('node');
    foreach ($nodes as $node) {

      // Build the view object and render the node.
      $view      = $view_builder->view($node, $this->view_mode);
      $outputs[] = $this->renderer->render($view);
    }

    return $outputs;
  }

}
