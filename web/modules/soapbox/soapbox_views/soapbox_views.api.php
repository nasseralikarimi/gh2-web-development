<?php

/**
 * @file
 * Soapbox Featured Cards API documentation.
 */

use Drupal\Core\Entity\Query\QueryInterface;

/**
 * Add classes to the filter buttons.
 *
 * @param array|object $classes
 *   The button classes.
 * @param string $button_type
 *   Button type - reset|submit.
 */
function hook_soapbox_views_filter_button_classes_alter(&$classes, &$button_type) {
  if ($button_type === 'reset') {
    $classes[] = 'some-class';
  }
  elseif ($button_type === 'submit') {
    $classes[] = 'some-submit-class';
  }
}

/**
 * Implements soapbox_views_filter_field_attributes_alter().
 */
function hook_soapbox_views_filter_field_attributes_alter(&$settings, &$option)
{
  // Add a special class to a specific field's wrapper.
  if ($settings[$option]['#title'] === 'Some title') {
    $settings[$option]['#wrapper_attributes']['class'][] = 'hello';
  }
}
