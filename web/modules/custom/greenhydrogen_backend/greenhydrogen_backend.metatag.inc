<?php

use Drupal\node\NodeInterface;

/**
 * Implements hook_metatags_alter().
 */
function greenhydrogen_backend_metatags_alter(array &$metatags, array &$context) {
  // Update twitter image meta tag.
  $node = \Drupal::routeMatch()->getParameter('node');

  if ($node && $node instanceof NodeInterface) {
    if ($node->hasField('field_image') && $node->get('field_image')->target_id) {
      $metatags["twitter_cards_image"] = '[node:field_image:entity:field_media_image:og_image:url]';
    }
    else {
      $metatags["twitter_cards_image"] = '[site:url]themes/path/to/your/fallback/png';
    }
  }
}
