<?php

namespace Drupal\greenhydrogen_backend\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Form\UserLoginForm;
use Drupal\user\UserInterface;

/**
 * Provides a user login form for Locked contend viewers.
 */
class LockedContentViewerUserForm extends UserLoginForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'locked_content_viewer_login_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('system.site');

    // Wrap the form so we can style it safely.
    $form['#prefix'] = '<div class="c-login-page"><div class="c-login-page__inner">';
    $form['#suffix'] = '</div></div>';

    $form['title'] = [
      '#markup' => '<h1 class="c-login-page__title">Login</h1>',
    ];

    $form['close'] = [
      '#markup' => '<a class="c-login-page__close js-close" href="#" title="Go back"></a>',
    ];

    // Display login form:
    // Hides username fields and sets it to the locked content viewer account.
    $form['name'] = [
      '#type'        => 'textfield',
      '#title'       => $this->t('Username'),
      '#size'        => 60,
      '#maxlength'   => UserInterface::USERNAME_MAX_LENGTH,
      '#description' => $this->t('Enter your @s username.', ['@s' => $config->get('name')]),
      '#required'    => TRUE,
      '#value'       => 'unlocker',
      '#access'      => FALSE,
      '#attributes'  => [
        'autocorrect'    => 'none',
        'autocapitalize' => 'none',
        'spellcheck'     => 'false',
        'autofocus'      => 'autofocus',
      ],
    ];

    $form['pass'] = [
      '#type'        => 'password',
      '#title'       => $this->t('Password'),
      '#size'        => 60,
      // SBTODO: Do we have real text for here?
      '#description' => $this->t('Enter the password to unlock content.'),
      '#required'    => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['back'] = [
      '#markup' => '<a class="c-login-page__go-back js-go-back" href="#">Go back</a>',
    ];

    $form['actions']['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Sign-in'),
    ];

    $form['#validate'][] = '::validateName';
    $form['#validate'][] = '::validateAuthentication';
    $form['#validate'][] = '::validateFinal';

    $this->renderer->addCacheableDependency($form, $config);

    return $form;
  }

}
