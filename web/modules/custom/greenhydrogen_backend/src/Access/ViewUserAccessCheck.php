<?php

namespace Drupal\greenhydrogen_backend\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Determines access for User edit page.
 */
class ViewUserAccessCheck implements AccessInterface {

  /**
   * Checks access to user page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultReasonInterface
   *   The access result.
   */
  public function access(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, "view user page");
  }

}
