<?php

namespace Drupal\greenhydrogen_backend\EventSubscriber;

use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\HttpFoundation\Cookie;

/**
 * Provides an exception subscriber.
 *
 * Responds to html format and implements handler for the 403 error code
 * when anonymous users try to access locked content.
 *
 * @package Drupal\greenhydrogen_backend\EventSubscriber
 */
class RedirectOn403Subscriber extends HttpExceptionSubscriberBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * RedirectOn403Subscriber constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(AccountInterface $current_user) {
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats() {
    return ['html'];
  }

  /**
   * Fallback for 403 HTTP response code.
   *
   * @param \Symfony\Component\HttpKernel\Event\ExceptionEvent $event
   *   The event to process.
   */
  public function on403(ExceptionEvent $event) {
    $node = \Drupal::routeMatch()->getParameter('node');
    $is_locked = FALSE;

    // Check if node is locked.
    if ($node instanceof NodeInterface) {
      if ($node->hasField('field_access')) {
        $access = $node->get('field_access')->referencedEntities();
        $is_locked = $access[0]->getName() == 'Locked content';
      }
    }

    $is_anonymous = $this->currentUser->isAnonymous();

    // If user is anonymous and content is locked redirect to login page.
    if ($is_anonymous && $is_locked) {
      $returnResponse = new RedirectResponse('/blocker');
      $cookie = new Cookie('locked_node', $node->id());
      $returnResponse->headers->setCookie($cookie);
      $event->setResponse($returnResponse);
    }
  }

}
