<?php

namespace Drupal\greenhydrogen_backend\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $user_view_route = $collection->get('entity.user.canonical');
    $user_edit_route = $collection->get('entity.user.edit_form');
    if ($user_edit_route) {
      $user_edit_route->setRequirements([
        '_edit_user_access_check' => 'TRUE',
      ]);
    }
    if ($user_view_route) {
      $user_view_route->setRequirements([
        '_view_user_access_check' => 'TRUE',
      ]);
    }
  }

}