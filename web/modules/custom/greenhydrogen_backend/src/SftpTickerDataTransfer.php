<?php

namespace Drupal\greenhydrogen_backend;

use Drupal\Core\Site\Settings;
use Drupal\file\Entity\File;
use phpseclib3\Net\SFTP;
use Psr\Log\LoggerInterface;

/**
 * Defines the  SftpTickerDataTransfer class.
 *
 * Performing actions to connect and transfer the GreenHydrogen ticker data
 * file trough SFTP.
 */
class SftpTickerDataTransfer {

  /**
   * The directory to the save the data.
   *
   * @var string
   */
  protected $dir = 'private://sftp_ticker_data';

  /**
   * The local filename.
   *
   * @var string
   */
  protected $filename = 'sftp_ticker_data.csv';

  /**
   * The SFTP settings.
   *
   * @var array
   */
  protected $sftpSettings = [];

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs the SftpTickerDataTransfer instance.
   *
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings service.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger factory service.
   */
  public function __construct(
    Settings $settings,
    LoggerInterface $logger
  ) {
    $this->sftpSettings = $settings::get('greenhydrogen_sftp_settings');
    $this->logger = $logger;
  }

  /**
   * Establishes the SFTP connection.
   *
   * @return \phpseclib3\NET\SFTP|null|bool
   *   The SFTP connection, null or false if connection failed.
   */
  private function connect() {

    if (!empty($this->sftpSettings)) {
      // New SFTP connection.
      return new SFTP(
        $this->sftpSettings['server']['hostname'],
        $this->sftpSettings['server']['port']);
    }
    else {
      $this->logger->error(
        'Couldn\'t establish SFTP connection. Please set the SFTP credentials in the settings.php file.'
      );
      return FALSE;
    }
  }

  /**
   * Login with the SFTP credentials.
   *
   * @return \phpseclib3\NET\SFTP|bool
   *   The active connection on successful login, FALSE otherwise.
   */
  private function login() {

    $connection = $this->connect();
    if ($connection) {

      if (!$connection->login($this->sftpSettings['server']['username'], $this->sftpSettings['server']['password'])) {

        $this->logger->error(
          'Couldn\'t establish SFTP connection. Please check the SFTP credentials.'
        );
        return FALSE;

      }

      return $connection;
    }
    return FALSE;
  }

  /**
   * Gets the ticker data file and saves it locally (private file system).
   *
   * @return \Drupal\Core\Entity\EntityInterface|bool
   *   The file or false if an error occurred.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getTickerData() {
    if ($connection = $this->login()) {
      $connection->chdir($this->sftpSettings['file']['directory']);

      if ($connection && $connection->get($this->sftpSettings['file']['name'])) {
        $destination = $this->dir . '/' . $this->filename;
        $connection->get($this->sftpSettings['file']['name'], $destination);
      }
      else {
        $this->logger->error('Couldn\'t get SFTP Ticker data. The file doesn\'t exist in the server.');
        return FALSE;
      }
    }
    else {
      $this->logger->error('Couldn\'t get SFTP Ticker data. An unknown error occurred, please check the error log.');
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Gets the ticker data file stored locally.
   *
   * @return array|false
   *   The local ticker data file as an array, FALSE if not found
   */
  public function getLocalTickerData() {
    return file($this->dir . '/' . $this->filename);
  }

}
