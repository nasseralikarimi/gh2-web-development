<?php

use Drupal\Core\Form\FormState;
use Drupal\Core\Url;

/**
 * Implements hook_form_alter().
 */
function greenhydrogen_backend_form_alter(&$form, FormState &$form_state, $form_id) {
  if ($form_id == 'locked_content_viewer_login_form') {
    // Locked content login form.
    $request = \Drupal::request();
    $locked_node_id = $request->cookies->get('locked_node');

    // Redirect to last accessed locked node - saved as a cookie.
    if ($locked_node_id) {
      $options = ['absolute' => TRUE];
      $url = Url::fromRoute('entity.node.canonical', ['node' => $locked_node_id], $options);
      $request->query->set('destination', $url->toString());
    }
  }
}
