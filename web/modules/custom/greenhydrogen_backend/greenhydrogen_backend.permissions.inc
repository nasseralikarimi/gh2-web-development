<?php

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;

/**
 * Implements hook_node_access().
 */
function greenhydrogen_backend_node_access(NodeInterface $node, $operation, AccountInterface $account) {
  // Removes permission to view people pages.
  // (people content type is only used by the people component)
  $access_result = AccessResult::neutral();

  /** @var NodeInterface $current_node */
  $current_node      = \Drupal::routeMatch()->getParameter('node');
  $current_node_type = '';

  if ($current_node instanceof NodeInterface) {
    // The content type of the currently visited node.
    // We want to keep the 'view' access if the user is not trying to access the people page.
    // The people item is referenced by components available on Homepage and Page.
    $current_node_type = $current_node->getType();
  }

  // People Node types.
  if ('view' === $operation && $node->getType() === 'person' && $current_node_type === 'person') {
    // Return view access forbidden if user is not administrator.
    if (!$account->hasPermission('view person pages')) {
      $access_result = AccessResult::forbidden();
    }
  }

  // Other node types.
  /*
   * We can access the view mode, but that's okay as we only want to
   * restrict access to the full node view mode, so we can check
   * if $current_node (matched from the URL) matches the $node
   * (from the access check).
   */
  if (!empty($current_node) && ($current_node instanceof NodeInterface)  && $current_node->id() === $node->id() && $node->hasField('field_access')) {
    $access_value = $node->get('field_access')->getString();

    // Current user roles.
    $roles = $account->getRoles();

    // Roles that can access locked content.
    $privileged_roles = [
      'locked_content_viewer',
      'administrator',
    ];

    if (!empty($access_value)) {
      $term = \Drupal::entityTypeManager()
                     ->getStorage('taxonomy_term')
                     ->load($access_value);

      $name = $term->label();

      // If the node is locked, and the user doesn't have a privileged role
      // Block them!
      if (strtolower($name) === 'locked content' && empty(array_intersect($privileged_roles, $roles))) {
        $access_result = AccessResult::forbidden();
      }
    }
  }

  return $access_result;
}
