<?php

namespace Drupal\content_sync\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for content_sync routes.
 */
class ContentSyncController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
