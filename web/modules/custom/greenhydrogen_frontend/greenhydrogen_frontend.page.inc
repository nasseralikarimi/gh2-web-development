<?php

/**
 * Implements hook_theme_suggestions_node_alter().
 */
function greenhydrogen_frontend_theme_suggestions_page_alter(
  array &$suggestions,
  array $variables
) {
  $title = trim(strip_tags($variables['page']['#title']));

  if (strtolower($title) === 'blocker') {
    $suggestions[] = 'page__blocker';
  }

  return $suggestions;
}
