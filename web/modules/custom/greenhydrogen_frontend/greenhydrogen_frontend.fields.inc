<?php

/**
 * @file
 * Provides additional variables for the paragraph templates.
 */

// use \Drupal\node\Entity\Node;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements template_preprocess_paragraph().
 */

//function greenhydrogen_frontend_preprocess_field(&$variables) {
//  /** @var \Drupal\node\Entity\Node $node */
//  $node_type  = $variables['element']['#bundle'];
//  $field_name = $variables['element']['#field_name'];
//
//  if ($node_type === 'home_page' && $field_name == 'field_carousel') {
//    /** @var \Drupal\greenhydrogen_frontend\CarouselHelperService $carousel_helper */
//    $carousel_helper = Drupal::service('greenhydrogen_frontend.carousel');
//    $carousel_helper->preprocessCarousel($variables);
//  }
//}

/**
 * Preprocess fields
 *
 * @param $variables
 */
function greenhydrogen_frontend_preprocess_field(&$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node_type  = $variables['element']['#bundle'];
  $field_name = $variables['element']['#field_name'];

  // Surface the media type to the field template.
  if ($node_type === 'home_page' && in_array(
          $field_name,
          [
              'field_banner_image',
              'field_banner_video',
          ]
      )) {
    if (!empty($variables['items'][0]['content']['#media'])) {
      /** @var \Drupal\media\Entity\Media $node */
      $media = $variables['items'][0]['content']['#media'];
      $type  = $media->bundle();

      $variables['media_type'] = $type;

      // If it's a remote video, get the Youtube ID.
      if ($type === 'remote_video') {
        /** @var \Drupal\Core\Field\FieldItemList $oembed */
        $oembed = $media->get('field_media_oembed_video');
        $url    = $oembed->getString();

        if (!empty($url)) {
          parse_str(parse_url($url, PHP_URL_QUERY), $url_vars);

          $variables['youtube_video_id'] = $url_vars['v'];
        }
      }
    }
  }
}
