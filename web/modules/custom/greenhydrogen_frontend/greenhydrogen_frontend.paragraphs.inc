<?php

/**
 * @file
 * Provides additional variables for the paragraph templates.
 */

// use \Drupal\node\Entity\Node;
use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implements template_preprocess_paragraph().
 */
function greenhydrogen_frontend_preprocess_paragraph(&$variables) {
  /** @var \Drupal\paragraphs\Entity\Paragraph $paragraph */
  $paragraph = $variables['paragraph'];

  if ($node = $paragraph->getParentEntity()) {
    // Adding node information as a render var to paragraphs.
    $variables['node'] = $node;

    // Add details for specific paragraph types.
    switch ($paragraph->getType()) {
      case 'example':
        $titles = [];

        if ($paragraph instanceof EntityInterface && $paragraph->hasField('field_tabs')) {
          foreach ($paragraph->field_tabs as $reference) {
            /** @var \Drupal\paragraphs\Entity\Paragraph $tab */
            $tab = $reference->entity;

            if ($tab instanceof EntityInterface && $tab->hasField('field_title')) {
              $titles[] = $tab->field_title->getString();
            }
          }
        }

        $variables['tab_titles'] = $titles;

        break;
    }
  }
}
