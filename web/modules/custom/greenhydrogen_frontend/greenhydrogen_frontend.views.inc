<?php

/**
 * @file
 * Provides additional variables for the views templates.
 */

use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\views\ViewExecutable;

/**
 * Alter view query to change conditions.
 *
 * @param \Drupal\views\ViewExecutable $view
 *   View to alter.
 * @param \Drupal\views\Plugin\views\query\QueryPluginBase $query
 *   Query to alter.
 */
function greenhydrogen_frontend_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->id() === 'search') {
    // Exclude 404, 403 from search result.
    $page_config = \Drupal::config('system.site')->get('page');
    $pages_to_check = ['404', '403', 'front'];

    foreach ($pages_to_check as $page) {
      if (!empty($page_config[$page])) {
        $exploded = explode('/', $page_config[$page]);

        if (!empty($exploded[2])) {
          $exclude_nids[] = $exploded[2];
        }
      }
    }

    if (!empty($exclude_nids)) {
      // Remove the node IDs from the search indexed query.
      $fields = $query->getIndex()->getFields();

      if (!empty($fields['nid'])) {
        $query->addCondition('nid', $exclude_nids, 'NOT IN');
      }
    }
  }
}
