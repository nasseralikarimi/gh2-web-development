<?php

use Drupal\node\NodeInterface;
use Drupal\node\Entity\Node;

/**
 * Implements template_preprocess_node().
 */
function greenhydrogen_frontend_preprocess_node(array &$variables) {

  // Content Type Preprocessing.
  _gho_frontend_preprocess_home_page($variables);

  // Component Preprocessing.
  _gho_frontend_preprocess_sub_pages($variables);
}

/**
 * Implements hook_theme_suggestions_node_alter().
 *
 * Prioritise certain node views over the content-type specific equivalents.
 *
 * e.g. node--hero is more important than node--event
 */
function greenhydrogen_frontend_theme_suggestions_node_alter(
  array &$suggestions,
  array $variables
) {
  // Priority Templates that should be moved in the array.
  $priority_view_modes = [
    'card',
    'card_full_width',
    'card_half',
    'card_one_third',
    'card_one_quarter',
    'card_sidebar',
  ];
  $view_mode = $variables['elements']['#view_mode'];

  // Check if we're currently outputting one of our priority view modes.
  if (in_array($view_mode, $priority_view_modes)) {
    $node = $variables['elements']['#node'];

    // We only want to be adjusting this for nodes.
    if ($node instanceof NodeInterface) {
      // Find our priority view modes array key.
      $key = array_search('node__' . $view_mode, $suggestions);

      if ($key !== FALSE) {
        // Remove it from the array.
        unset($suggestions[$key]);

        // Add our priority view mode back in at the end of the array.
        $suggestions[] = 'node__' . $view_mode;
      }
    }
  }

  return $suggestions;
}

/**
 * Preprocess the home page.
 * - Prepare the video embed for the banner.
 *
 * @param $variables
 */
function _gho_frontend_preprocess_home_page(&$variables) {
  /** @var \Drupal\node\Entity\Node $node */
  $node = $variables['node'];

  if ($node instanceof NodeInterface && $node->bundle() === 'home_page') {
    // Surface the Youtube ID if a banner video has been added.
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemList $video */
    $video = $node->get('field_banner_video');

    if (!empty($video)) {
      /** @var \Drupal\media\Entity\Media $media */
      $media = $video->entity;

      if (!empty($media) && $media->hasField('field_media_oembed_video')) {
        $oembed = $media->get('field_media_oembed_video');

        if (!empty($oembed)) {
          $url = $oembed->getString();

          if (!empty($url)) {
            parse_str(parse_url($url, PHP_URL_QUERY), $url_vars);

            $variables['youtube_video_id'] = $url_vars['v'];
            $variables['has_video_id'] = TRUE;
          }
        }
      }
    }

    // Get the ticker data file stored locally.
    $data = \Drupal::service('greenhydrogen_backend.sftp_ticker_data_transfer')
                   ->getLocalTickerData();
    if ($data) {
      $variables['ticker_data'] = $data;
    }
  }
}


/**
 * Preprocess the sub pages to be output in the subnav.
 *
 * @param $variables
 *
 * @throws \Drupal\Core\Entity\EntityMalformedException
 */
function _gho_frontend_preprocess_sub_pages(&$variables) {
  $node = $variables['node'];

  if (
    $node instanceof NodeInterface &&
    $variables['view_mode'] === 'full' &&
    in_array(
      $node->bundle(),
      [
        'home_page',
        'rich_content_page',
        'basic_page',
      ]
    )
  ) {

    /** @var \Drupal\soapbox_nodes\SubPagesHelperService $sub_pages_helper */
    $sub_pages_helper = Drupal::service('soapbox_nodes.sub_pages');
    $sub_page_ids = $sub_pages_helper->getSubPages($node);

    if (!empty($sub_page_ids)) {
      // The below will generate the node objects from node ids.
      // You can directly assign it to $variables if it can be processed form the twig file.
      $sub_pages = Node::loadMultiple($sub_page_ids);

      // Alternatively, generate an array of URL strings and labels.
      if ($sub_pages) {
        $sub_nav_items = [];

        foreach ($sub_pages as $sub_page) {
          $sub_nav_items[] = [
            'node' => $sub_page,
            'id' => $sub_page->id(),
            'url' => $sub_page->toUrl()->toString(),
            'title' => $sub_page->label(),
          ];
        }

        $variables['sub_pages'] = $sub_nav_items;
      }
    }
  }
}
