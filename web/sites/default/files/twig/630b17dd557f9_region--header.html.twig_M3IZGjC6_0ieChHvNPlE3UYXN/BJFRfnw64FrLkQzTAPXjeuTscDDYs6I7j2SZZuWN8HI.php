<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/greenhydrogen/templates/region/region--header.html.twig */
class __TwigTemplate_3a0954e43e771d5bb14fde0c0958e1b7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<header class=\"c-header js-header\" id=\"js-header\">
  <div class=\"c-header__container o-container\">
    <div class=\"c-header__inner\">
      ";
        // line 5
        echo "      <div class=\"c-header__condensed\">
        <a href=\"/\" class=\"c-header__logo\" title=\"";
        // line 6
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Return to the homepage"));
        echo "\">
          ";
        // line 7
        $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--header.html.twig", 7)->display(twig_array_merge($context, ["svg" => "logo", "extra_class" => "c-header__logo-icon-sticky"]));
        // line 8
        echo "          ";
        $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--header.html.twig", 8)->display(twig_array_merge($context, ["svg" => "logo-with-shapes", "extra_class" => "c-header__logo-icon-shapes"]));
        // line 9
        echo "        </a>

        <button class=\"c-header__burger js-header-burger\" aria-label=\"Toggle mobile menu\">
          ";
        // line 12
        $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--header.html.twig", 12)->display(twig_array_merge($context, ["svg" => "burger-icon", "extra_class" => "c-header__burger-icon c-header__burger-icon--menu"]));
        // line 13
        echo "        </button>
      </div>

      <div class=\"c-header__content js-main-menu\">
        <div class=\"c-header__content-top\">
          ";
        // line 19
        echo "          ";
        if (($context["logged_in"] ?? null)) {
            // line 20
            echo "            <a class=\"c-header__button\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("user.logout"));
            echo "\">
              ";
            // line 21
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Logout"));
            echo "
            </a>
          ";
        } else {
            // line 24
            echo "            <a class=\"c-header__button\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, (($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("greenhydrogen_backend.login") . "?destination=") . $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("<current>")), "html", null, true);
            echo "\">
              ";
            // line 25
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Sign in"));
            echo "
            </a>
          ";
        }
        // line 28
        echo "
          <button class=\"c-header__burger js-header-burger\" aria-label=\"Toggle mobile menu\">
            ";
        // line 30
        $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--header.html.twig", 30)->display(twig_array_merge($context, ["svg" => "close-icon", "extra_class" => "c-header__burger-icon c-header__burger-icon--close"]));
        // line 31
        echo "          </button>
        </div>

        <div class=\"c-header__content-inner\">
          <div class=\"c-header__top\">
            <div class=\"c-header__supplementary-menu\">
              ";
        // line 37
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["elements"] ?? null), "supplementarymenu", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
        echo "
            </div>

            <div class=\"c-header__buttons\">
              ";
        // line 42
        echo "              ";
        // line 43
        echo "              ";
        if (($context["logged_in"] ?? null)) {
            // line 44
            echo "                <a class=\"c-header__button\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("user.logout"));
            echo "\">
                  ";
            // line 45
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Logout"));
            echo "
                </a>
              ";
        } else {
            // line 48
            echo "                <a class=\"c-header__button\" href=\"";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, (($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("greenhydrogen_backend.login") . "?destination=") . $this->extensions['Drupal\Core\Template\TwigExtension']->getPath("<current>")), "html", null, true);
            echo "\">
                  ";
            // line 49
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Sign in"));
            echo "
                </a>
              ";
        }
        // line 52
        echo "
              <button class=\"c-header__button c-header__button--search js-search-open\" aria-label=\"Search\">
                ";
        // line 54
        $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--header.html.twig", 54)->display(twig_array_merge($context, ["svg" => "search-icon", "extra_class" => "c-header__burger-icon c-header__search-icon"]));
        // line 55
        echo "              </button>
            </div>
            ";
        // line 58
        echo "            ";
        $this->loadTemplate("@greenhydrogen/region-partials/region--header-search.html.twig", "themes/custom/greenhydrogen/templates/region/region--header.html.twig", 58)->display($context);
        // line 59
        echo "          </div>

          ";
        // line 62
        echo "          <div class=\"c-header__main-menu\" id=\"js-main-menu\">
            ";
        // line 63
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["elements"] ?? null), "greenhydrogen_main_menu", [], "any", false, false, true, 63), 63, $this->source), "html", null, true);
        echo "
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/greenhydrogen/templates/region/region--header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 63,  160 => 62,  156 => 59,  153 => 58,  149 => 55,  147 => 54,  143 => 52,  137 => 49,  132 => 48,  126 => 45,  121 => 44,  118 => 43,  116 => 42,  109 => 37,  101 => 31,  99 => 30,  95 => 28,  89 => 25,  84 => 24,  78 => 21,  73 => 20,  70 => 19,  63 => 13,  61 => 12,  56 => 9,  53 => 8,  51 => 7,  47 => 6,  44 => 5,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/greenhydrogen/templates/region/region--header.html.twig", "/opt/lampp/htdocs/green-hydrogen-standard/web/themes/custom/greenhydrogen/templates/region/region--header.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("include" => 7, "if" => 19);
        static $filters = array("t" => 6, "trans" => 21, "escape" => 24);
        static $functions = array("path" => 20);

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if'],
                ['t', 'trans', 'escape'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
