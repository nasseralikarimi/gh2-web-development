<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @greenhydrogen/svg/svg.html.twig */
class __TwigTemplate_aac6802e3ec9391d0f1e8203aea6fdd6 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["extra_class"] = (( !twig_test_empty(($context["extra_class"] ?? null))) ? ((" " . $this->sandbox->ensureToStringAllowed(($context["extra_class"] ?? null), 1, $this->source))) : (""));
        // line 2
        if (twig_test_empty(($context["directory"] ?? null))) {
            // line 3
            echo "  ";
            $context["directory"] = "themes/custom/greenhydrogen";
        }
        // line 5
        echo "
";
        // line 6
        if ((($context["base_path"] ?? null) == "/")) {
            // line 7
            echo "  ";
            $context["base_path"] = "";
        }
        // line 9
        echo "
<svg class=\"o-svg o-svg--";
        // line 10
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["svg"] ?? null), 10, $this->source), "html_attr");
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["extra_class"] ?? null), 10, $this->source), "html", null, true);
        echo "\">
  <use class=\"o-svg__use\" xlink:href=\"/";
        // line 11
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, ($this->sandbox->ensureToStringAllowed(($context["base_path"] ?? null), 11, $this->source) . $this->sandbox->ensureToStringAllowed(($context["directory"] ?? null), 11, $this->source)), "html", null, true);
        echo "/assets/img/symbol/svg/sprite.symbol.svg?v=11#";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["svg"] ?? null), 11, $this->source), "html_attr");
        echo "\"/>
</svg>
";
    }

    public function getTemplateName()
    {
        return "@greenhydrogen/svg/svg.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 11,  59 => 10,  56 => 9,  52 => 7,  50 => 6,  47 => 5,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@greenhydrogen/svg/svg.html.twig", "/opt/lampp/htdocs/green-hydrogen-standard/web/themes/custom/greenhydrogen/templates/svg/svg.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 2);
        static $filters = array("e" => 10, "escape" => 10);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['e', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
