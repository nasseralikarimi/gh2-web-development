<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/greenhydrogen/templates/node/node--press.html.twig */
class __TwigTemplate_3cc2b36a5d5ee368a6bf5096b8e10d45 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["classes"] = [0 => "c-node", 1 => ("c-node--type-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source,         // line 3
($context["node"] ?? null), "bundle", [], "any", false, false, true, 3), 3, $this->source)))];
        // line 5
        echo "
<article";
        // line 6
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method", false, false, true, 6), 6, $this->source), "html", null, true);
        echo ">
\t";
        // line 7
        if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_press_release", [], "any", false, false, true, 7), "value", [], "any", false, false, true, 7) == true)) {
            // line 8
            echo "\t\t<div class=\"press row d-inline-box\">
\t\t\t<div class=\"col-md-4\">
\t\t\t\t";
            // line 10
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_image", [], "any", false, false, true, 10), 10, $this->source), "html", null, true);
            echo "
\t\t\t</div>
\t\t\t<div class=\"col-md-8 d-flex flex-column justify-content-center\">
\t\t\t\t<div class=\"press-release-details\">
\t\t\t\t\t<h3>Press Release</h3>
\t\t\t\t\t<h1 class=\"mb-4\">
\t\t\t\t\t\t<a href=\"";
            // line 16
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null), 16, $this->source), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t";
            // line 17
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 17, $this->source), "html", null, true);
            echo "
\t\t\t\t\t\t</a>
\t\t\t\t\t</h1>
\t\t\t\t\t<div class=\"press-body\">
\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t";
            // line 22
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_body", [], "any", false, false, true, 22), "summary", [], "any", false, false, true, 22), 22, $this->source), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"body\">
\t\t\t\t\t\t\t";
            // line 25
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_body", [], "any", false, false, true, 25), 25, $this->source), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            // line 27
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_file", [], "any", false, false, true, 27))) {
                // line 28
                echo "
\t\t\t\t\t\t\t<div class=\"mt-4 readmore-button file\">
\t\t\t\t\t\t\t\t<a href=\"";
                // line 30
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_file", [], "any", false, false, true, 30), 30, $this->source), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\tDownload PDF
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            } else {
                // line 35
                echo "\t\t\t\t\t\t\t<div class=\"mt-4 readmore-button url\">
\t\t\t\t\t\t\t\t<a href=\"";
                // line 36
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null), 36, $this->source), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\tRead more
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            // line 41
            echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t";
        } else {
            // line 46
            echo "\t\t<div class=\"press col-md-12 d-inline-box\">
\t\t\t<div class=\"col-md-6\">
\t\t\t\t";
            // line 48
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_image", [], "any", false, false, true, 48), 48, $this->source), "html", null, true);
            echo "
\t\t\t</div>
\t\t\t<div class=\"col-md-6 d-flex flex-column justify-content-center\">
\t\t\t\t<div class=\"press-release-details\">
\t\t\t\t\t<h1 class=\"mb-4\">
\t\t\t\t\t\t<a href=\"";
            // line 53
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null), 53, $this->source), "html", null, true);
            echo "\">
\t\t\t\t\t\t\t";
            // line 54
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null), 54, $this->source), "html", null, true);
            echo "
\t\t\t\t\t\t</a>
\t\t\t\t\t</h1>
\t\t\t\t\t<div class=\"press-body\">
\t\t\t\t\t\t<div class=\"summary\">
\t\t\t\t\t\t\t";
            // line 59
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_body", [], "any", false, false, true, 59), "summary", [], "any", false, false, true, 59), 59, $this->source), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"body\">
\t\t\t\t\t\t\t";
            // line 62
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_body", [], "any", false, false, true, 62), 62, $this->source), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            // line 64
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "field_file", [], "any", false, false, true, 64))) {
                // line 65
                echo "
\t\t\t\t\t\t\t<div class=\"mt-4 readmore-button file\">
\t\t\t\t\t\t\t\t<a href=\"";
                // line 67
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content"] ?? null), "field_file", [], "any", false, false, true, 67), 67, $this->source), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\tDownload PDF
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            } else {
                // line 72
                echo "\t\t\t\t\t\t\t<div class=\"mt-4 readmore-button url\">
\t\t\t\t\t\t\t\t<a href=\"";
                // line 73
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null), 73, $this->source), "html", null, true);
                echo "\">
\t\t\t\t\t\t\t\t\tRead more
\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t";
            }
            // line 78
            echo "\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>


\t";
        }
        // line 85
        echo "

</article>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/greenhydrogen/templates/node/node--press.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 85,  179 => 78,  171 => 73,  168 => 72,  160 => 67,  156 => 65,  154 => 64,  149 => 62,  143 => 59,  135 => 54,  131 => 53,  123 => 48,  119 => 46,  112 => 41,  104 => 36,  101 => 35,  93 => 30,  89 => 28,  87 => 27,  82 => 25,  76 => 22,  68 => 17,  64 => 16,  55 => 10,  51 => 8,  49 => 7,  45 => 6,  42 => 5,  40 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/greenhydrogen/templates/node/node--press.html.twig", "/opt/lampp/htdocs/green-hydrogen-standard/web/themes/custom/greenhydrogen/templates/node/node--press.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "if" => 7);
        static $filters = array("clean_class" => 3, "escape" => 6);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
