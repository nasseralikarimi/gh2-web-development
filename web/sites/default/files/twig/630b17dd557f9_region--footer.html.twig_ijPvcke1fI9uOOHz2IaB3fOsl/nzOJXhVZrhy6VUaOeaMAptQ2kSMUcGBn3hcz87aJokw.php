<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/greenhydrogen/templates/region/region--footer.html.twig */
class __TwigTemplate_575c5cd1e6727b73d73185ad817557c7 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "
<footer class=\"c-footer\">
  <div class=\"o-container\">
    <div class=\"c-footer__inner c-footer__inner--top\">
      ";
        // line 20
        echo "      <a href=\"/\" class=\"c-footer__logo\" title=\"";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Return to the homepage"));
        echo "\">
        ";
        // line 21
        $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--footer.html.twig", 21)->display(twig_array_merge($context, ["svg" => "logo"]));
        // line 22
        echo "      </a>

      ";
        // line 24
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["elements"] ?? null), "greenhydrogen_footer", [], "any", false, false, true, 24), 24, $this->source), "html", null, true);
        echo "
    </div>

    <div class=\"c-footer__inner c-footer__inner--bottom\">
      <p class=\"c-footer__credit\">
        © Green Hydrogen Standard. ";
        // line 29
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("All rights reserved."));
        echo "<br/>
        ";
        // line 31
        echo "      </p>

      <div class=\"c-footer__socials\">
        ";
        // line 35
        echo "        ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["site_settings"] ?? null), "footer", [], "any", false, false, true, 35), "footer", [], "any", false, false, true, 35), "field_twitter_url", [], "any", false, false, true, 35))) {
            // line 36
            echo "          <a class=\"c-footer__social-button\"
             href=\"";
            // line 37
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["site_settings"] ?? null), "footer", [], "any", false, false, true, 37), "footer", [], "any", false, false, true, 37), "field_twitter_url", [], "any", false, false, true, 37), "url", [], "any", false, false, true, 37), 37, $this->source), "html", null, true);
            echo "\"
             title=\"";
            // line 38
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Twitter"));
            echo "\"
             target=\"_blank\">
            ";
            // line 40
            $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--footer.html.twig", 40)->display(twig_array_merge($context, ["svg" => "twitter"]));
            // line 41
            echo "          </a>
        ";
        }
        // line 43
        echo "
        ";
        // line 45
        echo "        ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["site_settings"] ?? null), "footer", [], "any", false, false, true, 45), "footer", [], "any", false, false, true, 45), "field_linkedin_url", [], "any", false, false, true, 45))) {
            // line 46
            echo "          <a class=\"c-footer__social-button\"
             href=\"";
            // line 47
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["site_settings"] ?? null), "footer", [], "any", false, false, true, 47), "footer", [], "any", false, false, true, 47), "field_linkedin_url", [], "any", false, false, true, 47), "url", [], "any", false, false, true, 47), 47, $this->source), "html", null, true);
            echo "\"
             title=\"";
            // line 48
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("LinkedIn"));
            echo "\"
             target=\"_blank\">
            ";
            // line 50
            $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--footer.html.twig", 50)->display(twig_array_merge($context, ["svg" => "linkedin"]));
            // line 51
            echo "          </a>
        ";
        }
        // line 53
        echo "
        ";
        // line 55
        echo "        ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["site_settings"] ?? null), "footer", [], "any", false, false, true, 55), "footer", [], "any", false, false, true, 55), "field_youtube_url", [], "any", false, false, true, 55))) {
            // line 56
            echo "          <a class=\"c-footer__social-button\"
             href=\"";
            // line 57
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["site_settings"] ?? null), "footer", [], "any", false, false, true, 57), "footer", [], "any", false, false, true, 57), "field_youtube_url", [], "any", false, false, true, 57), "url", [], "any", false, false, true, 57), 57, $this->source), "html", null, true);
            echo "\"
             title=\"";
            // line 58
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("YouTube"));
            echo "\"
             target=\"_blank\">
            ";
            // line 60
            $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--footer.html.twig", 60)->display(twig_array_merge($context, ["svg" => "youtube"]));
            // line 61
            echo "          </a>
        ";
        }
        // line 63
        echo "
        ";
        // line 65
        echo "        ";
        if ( !twig_test_empty(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["site_settings"] ?? null), "footer", [], "any", false, false, true, 65), "footer", [], "any", false, false, true, 65), "field_facebook_url", [], "any", false, false, true, 65))) {
            // line 66
            echo "          <a class=\"c-footer__social-button\"
             href=\"";
            // line 67
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["site_settings"] ?? null), "footer", [], "any", false, false, true, 67), "footer", [], "any", false, false, true, 67), "field_facebook_url", [], "any", false, false, true, 67), "url", [], "any", false, false, true, 67), 67, $this->source), "html", null, true);
            echo "\"
             title=\"";
            // line 68
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Facebook"));
            echo "\"
             target=\"_blank\">
            ";
            // line 70
            $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "themes/custom/greenhydrogen/templates/region/region--footer.html.twig", 70)->display(twig_array_merge($context, ["svg" => "facebook"]));
            // line 71
            echo "          </a>
        ";
        }
        // line 73
        echo "      </div>
    </div>
  </div>
</footer>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/greenhydrogen/templates/region/region--footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 73,  162 => 71,  160 => 70,  155 => 68,  151 => 67,  148 => 66,  145 => 65,  142 => 63,  138 => 61,  136 => 60,  131 => 58,  127 => 57,  124 => 56,  121 => 55,  118 => 53,  114 => 51,  112 => 50,  107 => 48,  103 => 47,  100 => 46,  97 => 45,  94 => 43,  90 => 41,  88 => 40,  83 => 38,  79 => 37,  76 => 36,  73 => 35,  68 => 31,  64 => 29,  56 => 24,  52 => 22,  50 => 21,  45 => 20,  39 => 15,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/custom/greenhydrogen/templates/region/region--footer.html.twig", "/opt/lampp/htdocs/green-hydrogen-standard/web/themes/custom/greenhydrogen/templates/region/region--footer.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("include" => 21, "if" => 35);
        static $filters = array("t" => 20, "escape" => 24, "trans" => 29);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['include', 'if'],
                ['t', 'escape', 'trans'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
