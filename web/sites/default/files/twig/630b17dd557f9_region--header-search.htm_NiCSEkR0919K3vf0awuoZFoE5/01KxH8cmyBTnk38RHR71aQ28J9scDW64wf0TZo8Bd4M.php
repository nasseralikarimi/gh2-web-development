<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @greenhydrogen/region-partials/region--header-search.html.twig */
class __TwigTemplate_361f7f56dae4fd47f336a1d2013f43f0 extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["search_name"] = "keywords";
        // line 2
        echo "<div class=\"c-header-search js-search-form\" role=\"dialog\" aria-labelledby=\"header-search-label\">
  <div class=\"c-header-search__container o-container\">
    <div class=\"c-header-search__inner\">
      <p class=\"u-accessible-hide\" id=\"header-search-label\">Search the site</p>

      <form class=\"c-header-search__form\" action=\"/search\" method=\"get\" role=\"search\" aria-label=\"Sitewide search\">
        <input class=\"c-header-search__keyword-input js-search-field\"
               title=\"";
        // line 9
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Enter the terms you wish to search for."));
        echo "\"
               placeholder=\"Search term\"
               type=\"search\"
               name=\"";
        // line 12
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["search_name"] ?? null), 12, $this->source), "html", null, true);
        echo "\" value=\"\" size=\"15\" maxlength=\"128\"
        />

        <button class=\"c-header-search__submit-button\" title=\"";
        // line 15
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Search all content"));
        echo "\" type=\"submit\"
                value=\"\">
          <span class=\"u-accessible-hide\">Search</span>
          ";
        // line 18
        $this->loadTemplate((("@" . $this->extensions['Drupal\soapbox_themes\TwigExtension\ThemeTwigExtension']->getThemeName()) . "/svg/svg.html.twig"), "@greenhydrogen/region-partials/region--header-search.html.twig", 18)->display(twig_array_merge($context, ["svg" => "search-icon"]));
        // line 19
        echo "        </button>
      </form>

      <button class=\"c-header-search__close js-search-close\" aria-label=\"Close search box\">
        ";
        // line 23
        $this->loadTemplate("@greenhydrogen/svg/svg.html.twig", "@greenhydrogen/region-partials/region--header-search.html.twig", 23)->display(twig_array_merge($context, ["svg" => "close-icon", "extra_class" => "c-header-search__close-icon"]));
        // line 24
        echo "      </button>
    </div>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@greenhydrogen/region-partials/region--header-search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 24,  76 => 23,  70 => 19,  68 => 18,  62 => 15,  56 => 12,  50 => 9,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@greenhydrogen/region-partials/region--header-search.html.twig", "/opt/lampp/htdocs/green-hydrogen-standard/web/themes/custom/greenhydrogen/templates/region-partials/region--header-search.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 1, "include" => 18);
        static $filters = array("trans" => 9, "escape" => 12);
        static $functions = array("get_theme" => 18);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'include'],
                ['trans', 'escape'],
                ['get_theme']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
