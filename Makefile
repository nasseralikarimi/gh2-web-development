gulp-build:
	cd web/themes/custom/greenhydrogen/assets/src/ && ./node_modules/gulp/bin/gulp.js build --production

gulp-watch:
	cd web/themes/custom/greenhydrogen/assets/src/ && ./node_modules/gulp/bin/gulp.js watch

gulp-build-sprite:
	cd web/themes/custom/greenhydrogen/assets/src/ && ./node_modules/gulp/bin/gulp.js build-svg-sprite

# Run this command pre-commit to ensure no dev tools are enabled.
setup-production:
	ddev exec drush pm-uninstall devel, kint, stage_file_proxy -y

# Enable development modules.
setup-dev:
	ddev exec drush pm-enable devel, kint, stage_file_proxy -y
	#ddev exec drush config-set stage_file_proxy.settings origin "http://SBTODO.COM" -y

# Remove git subrepos.
post-composer-subrepos-remove:
	# Removing git subrepo git folders for Cloudways. If you have accidentally committed one,
	# Please use `git rm --cached path/to/folder -r`.
	find ./web -type d -name ".git" | xargs rm -rf
	find ./web -name ".gitignore" | xargs rm -rf
	find ./web -name ".gitmodules" | xargs rm -rf
	find ./vendor -type d -name ".git" | xargs rm -rf
	find ./vendor -name ".gitignore" | xargs rm -rf
	find ./vendor -name ".gitmodules" | xargs rm -rf

# Deployment to cloudways
cloudways-production-deploy:
	ssh gho-cloudways 'cd ~/applications/production/public_html && git pull'
	ssh gho-cloudways 'cd ~/applications/production/public_html && ./vendor/drush/drush/drush deploy'

cloudways-staging-deploy:
	ssh gho-cloudways 'cd ~/applications/staging/public_html && git pull'
	ssh gho-cloudways 'cd ~/applications/staging/public_html && ./vendor/drush/drush/drush deploy'

# Symlinks in place on Cloudways:
# production -> nzhgfvnyaf
# staging -> kqxvfcxskd
